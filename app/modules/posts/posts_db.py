from bson.objectid import ObjectId
class PostDB:
    def __init__(self, conn):
        self.conn = conn

    def getPosts(self, username):
        return self.conn.find({'username': username})

    def createPost(self, post, username):
        self.conn.insert({'post':post, 'username': username})

    def deletePost(self, post_id):
    	self.conn.remove({'_id': ObjectId(post_id)})

    def updatePost(self, emoticon, post_id):
    	self.conn.update({'_id': ObjectId(post_id)},{'$set':{'emoticon': emoticon}})

    def getPostById(self, post_id):
        return self.conn.find({'_id': ObjectId(post_id)})
