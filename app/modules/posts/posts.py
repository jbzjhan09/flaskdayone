from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session
from datetime import datetime

mod = Blueprint('posts', __name__)

@mod.route('/')
def post_list():
    posts = g.postsdb.getPosts(session['username'])
    return render_template('posts/post.html', posts=posts, date=datetime.now().strftime('%B %d %y'))

@mod.route('/', methods=['POST'])
def create_post():
    new_post = request.form['new_post']
    g.postsdb.createPost(new_post, session['username'])
    flash('New post created!', 'create_post_success')
    return redirect(url_for('.post_list'))

@mod.route('/delete', methods=['POST'])
def delete_post():
    _id = request.form['id']
    g.postsdb.deletePost(_id)
    flash('Post successfully deleted.', 'delete_post')
    return redirect(url_for('.post_list'))

@mod.route('/update', methods=['POST'])
def update_post():
    emoticon = request.json['emoticon']
    post_id = request.json['id']
    g.postsdb.updatePost(emoticon, post_id)
    flash('Post updated', 'update_post')
    return render_template('posts/partial_form.html', emoticon=emoticon)
