function delete_post(form){
	return confirm('Are you sure you want to delete post?')
}

function choose_emoticon(field, post_id){
	emoticon = $(field).val();
    parent = $(field).parent();
    parent = $(parent).parent();
	$(parent).find('input').slice(1, 7).each(function(i, f){
		$(f).removeClass('selected');
	})

    object = {};
    object.emoticon = emoticon;
    object.id = post_id;

    $.ajax({
    	url: '/posts/update',
    	type: "POST",
    	data: JSON.stringify(object, null, '\t'),
    	contentType: 'application/json;charset=UTF-8',
    	success: function(reply){
			$(field).addClass('selected');
	        $(parent).find('a').first().html('-'+reply+'-');
    	}
    })
}